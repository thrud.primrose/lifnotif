#disclaimer for notify2
'''
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
'''

#for robobrowser
'''
Copyright (c) "", Joshua Carp
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

* Neither the name of robobrowser nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
'''

import re
import time
from robobrowser import RoboBrowser
import notify2
import subprocess as s
import os

def login():
  username = input("E-Mail/Username pazhalushta:\n")
  password = input("Password pazahlushta:\n")
  #there are 2 forms
  #but cannot get it if I use get_form(...)

  browser = RoboBrowser()
  browser.open('https://lichess.org/login?referrer=/')
  forms = browser.get_forms()
  form = forms[1]
  form["username"] = username
  form["password"] = password
  #no need for token probably
  browser.submit_form(form)
  html = str(browser.parsed())
  #print("connected desu~")


  #connected
  while(True):

    browser.open('https://lichess.org')
    src = str(browser.parsed())

    #friend-box friend names begins after start.str and ends before end.str
    start = "data-preload"
    end = "data-studying"

    result = re.search('%s(.*)%s' % (start, end), src)
    if(result==None):
      print("uwu problem, bcs to tried to login too much, I will wait 10 min\n")
      time.sleep(600)
      browser = RoboBrowser()
      browser.open('https://lichess.org/login?referrer=/')
      forms = browser.get_forms()
      form = forms[1]
      form["username"] = username
      form["password"] = password
      browser.submit_form(form)
      html = str(browser.parsed())
      continue

    groupedup = result.group(1)
    listofonline = groupedup.split(',')

    #check if empty (no friends online) or not (some friends online)
    if groupedup == '=""'  or groupedup == '="" ' or groupedup == '=""\n' or groupedup == '="" \n':
      empty = True
    else:
      empty = False

    #assign the string to print in notification
    toprt=""
    if not empty:
      i = 0
      toprt+="there are some people to smack, Naisu desu~: "
      while i < len(listofonline):
        toprt+=listofonline[i]
        toprt+=" "
        i += 1
    else:
      toprt+="there is nobody to slap, you lone weird unhappy incel."

    #create notification
    #no icon yet
    sum=""
    notify2.init('Lichess Notifier Desu')
    if empty:
      sum+="Found Desu~"
      n = notify2.Notification(sum,toprt,"")
      n.show()
    #couldnt sleep for more
    time.sleep(180)

if __name__ == "__main__":login()
